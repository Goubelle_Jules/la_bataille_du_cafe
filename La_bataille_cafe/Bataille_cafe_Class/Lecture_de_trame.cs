﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bataille_cafe
{
    public static class Lecture_de_trame
    {

        // verifie si le caractère est un chiffre 
        public static bool Is_Chiffre(string trame, int index)
        {
            if (trame[index] == ' ' || trame[index] == '|' || trame[index] == ':' || trame[index] == '\0')
                return false;
            else
                return true;


        }

        
        // a commenter par Tom
        public static void Remplissage_Du_Tableau(string[] entiers, string trame)
        {
            int remplissage = 0;


            for (int index = 0; index < trame.Length; index++)
            {

                if (Is_Chiffre(trame, index))
                {

                    if (index != 0 && Is_Chiffre(trame, index - 1))
                    {
                        remplissage--;
                        entiers[remplissage] = trame[index - 1].ToString() + trame[index].ToString();

                    }
                    else
                    {
                        entiers[remplissage] = trame[index].ToString();

                    }

                    remplissage++;

                }





            }

        }

        // a commenter par tom 
        public static int[] Tab_To_Int(string[] entiers)
        {
            int[] Map = new int[100];
            for (int index = 0; index < entiers.Length; index++)
            {
                Map[index] = Int32.Parse(entiers[index]);
            }

            return Map;

        }
    }
}
