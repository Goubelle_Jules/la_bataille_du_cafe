﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Bataille_cafe
{
    public static class DecodageMap
    {


        // retourne la valeur binaire inversé d'un entier
        public static int[] ToBinaire(int valeur)
        {
            int[] binaire = new int[4];
            for (int index = 0; index < 4; index++)
            {
                binaire[index] = valeur % 2;
                valeur = valeur / 2;
            }

            return binaire;
        }

        // permet la lecture des diffèrentes parcelle de la carte 

        public static int LectureParcelle(string[] decoder, int[] tab, int index, int value)
        {

            int[] binaire = ToBinaire(tab[index]);

            if (binaire[0] == 0 && decoder[index - 10] != "0")//si valeur au nord et case vide 
                decoder[index] = decoder[index - 10]; // prendre la valeur 
            else
                if (binaire[1] == 0 && decoder[index - 1] != "0")//si valeur à l'est et case vide 
                    decoder[index] = decoder[index - 1]; // prendre la valeur 
                else
                    if (binaire[2] == 0 && decoder[index + 10] != "0")//si valeur au sud et case vide 
                        decoder[index] = decoder[index + 10]; // prendre la valeur 
                    else
                        if (binaire[3] == 0 && decoder[index + 1] != "0")//si valeur a l'ouest et case vide 
                            decoder[index] = decoder[index + 1];// prendre la valeur 
                        else // si rien n'est affecté 
                            {
                            decoder[index] = char.ConvertFromUtf32(value); // prends une nouvelle valeur non existante
                            value++; //change la valeur non utiliser
                            }
            if (binaire[1] == 0 && decoder[index - 1] != "0") // si la valeur à l'est est vide mais appartient a la parcelle alors
                LectureParcelle(decoder, tab, index - 1, value);//on recommence la lecture de la case a l'est pour qu'elle prennes la bonne valeur


            return value;
        }


        // decode toute la carte 
        public static string[] Decodage(int[] tableau)
        {
            int value = 97;
            string[] decoder = new string[100];
            for (int index = 0; index < 100; index++)
            {
                decoder[index] = "0";
            }


            for (int index = 0; index < tableau.Length; index++)
            {
                if (tableau[index] > 64)
                    decoder[index] = "M";
                else
                {
                    if (tableau[index] > 30 && tableau[index] < 45)
                        decoder[index] = "F";

                    else
                    {
                        if (decoder[index] == "0")
                            value = LectureParcelle(decoder, tableau, index, value);

                    }
                }
            }



            return decoder;
        }




    }
}