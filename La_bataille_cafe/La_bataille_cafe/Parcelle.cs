﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace La_bataille_cafe
{
    public class Parcelle
    {
        public int nbcase;
        public int caseGagnante;
        public int caseJoueur=0;
        public int caseEnnemi=0;
        public List<int> coordonnees=new List<int>();
        public string lettre;

        public Parcelle(string p_lettre)
        {
            lettre = p_lettre;
        }

        public void IncCaseJoueur()
        {
            caseJoueur = 1+caseJoueur;
        }

        public void IncCaseEnnemi()
        {
            caseEnnemi = 1 + caseEnnemi;
        }

        public void DefinePointGagnat()
        {
            if (nbcase % 2 == 0)
            {
                caseGagnante = nbcase / 2 + 1;
            }
            else
            {
                caseGagnante = nbcase / 2;
            }
        }

        public void AddCoordonnées(int coor) {
            nbcase = nbcase + 1;
            coordonnees.Add(coor);
        }
        public void DownCoordonnees(int value)
        {
            coordonnees.Remove(value);
        }


    }
}
