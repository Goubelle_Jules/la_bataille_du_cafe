﻿using System;
using System.Collections.Generic;
using La_bataille_cafe;

namespace Bataille_cafe
{
    public static class Play
    {
        public static string choixducoup(string choixia,List<Parcelle> parcelles) {
            string choix="22";
            string parcellename="z";
            bool IsNotChoice = true;
            int coup=0;


            //premier coup
            if (choixia == "xx")
            {
                foreach (Parcelle parcelle in parcelles)
                {
                    if (parcelle.nbcase == 3 && IsNotChoice)
                    {
                        choix = parcelle.coordonnees[0].ToString();
                        parcelle.DownCoordonnees(parcelle.coordonnees[0]);
                        parcelle.IncCaseJoueur();
                        IsNotChoice = false;
                    }
                }
            }
            //les autres coups
            else {
                //retire les cases ou l'ia adverse joue
                foreach (Parcelle parcelle in parcelles)
                {
                   
                    foreach (int coordonnee in parcelle.coordonnees)
                    {
                        if (coordonnee.ToString()[0] == choixia[2] && coordonnee.ToString()[1] == choixia[3])
                        {
                            parcellename = parcelle.lettre;
                            coup = coordonnee;
                            
                            
                        }
                    }
                    parcelle.DownCoordonnees(coup);
                    parcelle.IncCaseEnnemi();

                }
                //cas ou le joueur peut valider une parcelle(1 point restant)
                foreach (Parcelle parcelle in parcelles)
                {
                    
                    if (parcelle.lettre != parcellename && IsNotChoice)
                    {

                        if (parcelle.caseJoueur < parcelle.caseGagnante && parcelle.caseGagnante-parcelle.caseJoueur==1 )
                        {

                            foreach (int coordonnee in parcelle.coordonnees)
                            {

                                if (coordonnee.ToString()[0] == choixia[2] || coordonnee.ToString()[1] == choixia[3])
                                {
                                
                                    choix = coordonnee.ToString();
                                    coup = coordonnee;
                                 
                                    IsNotChoice = false;

                                }


                            }
                            parcelle.DownCoordonnees(coup);
                            parcelle.IncCaseJoueur();
                        }
                    }

                }
                //cas ou le joueur peut valider une parcelle(2 point restant)
                foreach (Parcelle parcelle in parcelles)
                {
            
                    if (parcelle.lettre != parcellename && IsNotChoice)
                    {

                        if (parcelle.caseJoueur < parcelle.caseGagnante && parcelle.caseGagnante - parcelle.caseJoueur == 2 && parcelle.caseGagnante-parcelle.caseEnnemi!=0)
                        {

                            foreach (int coordonnee in parcelle.coordonnees)
                            {

                                if (coordonnee.ToString()[0] == choixia[2] || coordonnee.ToString()[1] == choixia[3])
                                {
                                 
                                    choix = coordonnee.ToString();
                                    coup = coordonnee;

                                    IsNotChoice = false;

                                }


                            }
                            parcelle.DownCoordonnees(coup);
                            parcelle.IncCaseJoueur();
                        }
                    }

                }
                //cas ou le joueur peut valider une parcelle(3 point restant)
                foreach (Parcelle parcelle in parcelles)
                {
           
                    if (parcelle.lettre != parcellename && IsNotChoice)
                    {

                        if (parcelle.caseJoueur < parcelle.caseGagnante && parcelle.caseGagnante - parcelle.caseJoueur == 3 && parcelle.caseGagnante - parcelle.caseEnnemi != 0)
                        {

                            foreach (int coordonnee in parcelle.coordonnees)
                            {

                                if (coordonnee.ToString()[0] == choixia[2] || coordonnee.ToString()[1] == choixia[3])
                                {
                          
                                    choix = coordonnee.ToString();
                                    coup = coordonnee;

                                    IsNotChoice = false;

                                }


                            }
                            parcelle.DownCoordonnees(coup);
                            parcelle.IncCaseJoueur();
                        }
                    }

                }
                //cas ou le joueur peut jouer dans une parcelle
                foreach (Parcelle parcelle in parcelles)
                {
               
                    if (parcelle.lettre != parcellename && IsNotChoice)
                    {

                        if (parcelle.caseJoueur < parcelle.caseGagnante)
                        {

                            foreach (int coordonnee in parcelle.coordonnees)
                            {

                                if (coordonnee.ToString()[0] == choixia[2] || coordonnee.ToString()[1] == choixia[3])
                                {
                                  
                                    choix = coordonnee.ToString();
                                    coup = coordonnee;

                                    IsNotChoice = false;

                                }


                            }
                            parcelle.DownCoordonnees(coup);
                            parcelle.IncCaseJoueur();
                        }
                    }

                }
                //cas ou l'ia peut valider une parcelle l'en empecher
                foreach (Parcelle parcelle in parcelles)
                {
             
                    if (parcelle.lettre != parcellename && IsNotChoice)
                    {

                        if (parcelle.caseGagnante-parcelle.caseEnnemi==1)
                        {

                            foreach (int coordonnee in parcelle.coordonnees)
                            {

                                if (coordonnee.ToString()[0] == choixia[2] || coordonnee.ToString()[1] == choixia[3])
                                {
                                   
                                    choix = coordonnee.ToString();
                                    coup = coordonnee;

                                    IsNotChoice = false;

                                }


                            }
                            parcelle.DownCoordonnees(coup);
                            parcelle.IncCaseJoueur();
                        }
                    }

                }
                //cas ou l'ia peut valider une parcelle l'en empecher
                foreach (Parcelle parcelle in parcelles)
                {
                    
                    if (parcelle.lettre != parcellename && IsNotChoice)
                    {

                        if (parcelle.caseGagnante - parcelle.caseEnnemi == 2)
                        {

                            foreach (int coordonnee in parcelle.coordonnees)
                            {

                                if (coordonnee.ToString()[0] == choixia[2] || coordonnee.ToString()[1] == choixia[3])
                                {
                                   
                                    choix = coordonnee.ToString();
                                    coup = coordonnee;

                                    IsNotChoice = false;

                                }


                            }
                            parcelle.DownCoordonnees(coup);
                            parcelle.IncCaseJoueur();
                        }
                    }

                }
                //cas ou l'ia peut valider une parcelle l'en empecher
                foreach (Parcelle parcelle in parcelles)
                {
              
                    if (parcelle.lettre != parcellename && IsNotChoice)
                    {

                        if (parcelle.caseGagnante - parcelle.caseEnnemi == 3)
                        {

                            foreach (int coordonnee in parcelle.coordonnees)
                            {

                                if (coordonnee.ToString()[0] == choixia[2] || coordonnee.ToString()[1] == choixia[3])
                                {
                                    
                                    choix = coordonnee.ToString();

                                    coup = coordonnee;

                                    IsNotChoice = false;

                                }


                            }
                            parcelle.DownCoordonnees(coup);
                            parcelle.IncCaseJoueur();
                        }
                    }

                }
                //cas ou je joue juste 
                foreach (Parcelle parcelle in parcelles)
                {
      
                    if (parcelle.lettre != parcellename && IsNotChoice)
                    {

                        

                        foreach (int coordonnee in parcelle.coordonnees)
                        {

                            if (coordonnee.ToString()[0] == choixia[2] || coordonnee.ToString()[1] == choixia[3])
                            {
                                Console.WriteLine("meme colonne/ligne");
                                choix = coordonnee.ToString();
                                coup = coordonnee;

                                IsNotChoice = false;

                            }


                        }
                        parcelle.DownCoordonnees(coup);
                        parcelle.IncCaseJoueur();

                    }

                }

            }
            return "A:" + choix;
        }
    }
}
