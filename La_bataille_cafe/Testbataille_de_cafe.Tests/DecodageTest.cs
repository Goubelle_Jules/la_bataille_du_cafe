﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bataille_cafe;


namespace Testbataille_de_cafe.Tests
{
    [TestClass]
    public class TestDecodage
    {
        [TestMethod]
        public void TobinaireTest()
        {
            int[] espere = new int[4];
            espere[0]= 1;
            espere[1] = 1;
            espere[2] = 1;
            espere[3] = 1;
            Assert.AreEqual(espere,Decodage.Tobinaire(15));
        }
    }
}
